# -*- coding: utf-8 -*-
import telebot
import codecs, json
import time, sys
token = sys.argv[1]
texts = []
order = dict()
bot = telebot.TeleBot(token)

def format_responce():
	resp_list = []
	for key in order.keys():
		resp_list.append("{0}: {1}".format(key.encode('utf8'), order[key]))
	return '\n'.join(resp_list)

def format_help():
	help = (u"Отправлять заказы нужно в формате <блюдо>: <количество> \n"
		u"/get - получить список заказов \n/clear - очистить список заказов")
	return help

def get_menu():
	menu = None
	with codecs.open("menu.json", 'r', encoding="utf-8") as f:
		menu = json.load(f, encoding="utf-8")
	data = []
	for key in menu.keys():
		element = u"{}\n === \n".format(key.upper())
		element += u"\n".join(menu[key])
		data.append(element)
	return data


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.reply_to(message, format_help())

@bot.message_handler(commands=['get'])
def get_command(message):
	bot.reply_to(message, format_responce())

@bot.message_handler(commands=['clear'])
def clear_command(message):
	order.clear()

@bot.message_handler(commands=['menu'])
def clear_command(message):
	menu = get_menu()
	for elem in menu:
		bot.reply_to(message, elem)

@bot.message_handler(content_types=['text'])
def text_handler(message):
	msg = message.text
	if ':' in msg:
		order_item = msg.split(':')
		if len(order_item) == 2:
			try:
				num = int(order_item[1])
				name = order_item[0].lower()
				if name not in order.keys():
					order[name] = num
				else:
					order[name] = order[name] + num
				if order [name] < 1:
					del order[name]
			except:
				bot.send_message(message.chat.id, "Error")
				



if __name__ == '__main__':
	bot.polling(none_stop=True)
